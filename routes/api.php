<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\PageController;
use App\Http\Controllers\API\PostController;
use App\Http\Controllers\API\UserFollowerController;
use App\Http\Controllers\API\UserFollowPageController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {*/
Route::post('/login', [AuthController::class, 'login']);
Route::post('/register', [AuthController::class, 'register']);
    Route::middleware('jwt.verify')->group(function (){

    Route::post('/logout', [AuthController::class, 'logout']);

    Route::get('/user-profile', [AuthController::class, 'userProfile']);
    Route::post('/pages', [PageController::class, 'store']);
    Route::post('/posts', [PostController::class, 'store']);
    Route::post('/page-posts', [PostController::class, 'store_page_post']);
    Route::post('/follow-user', [UserFollowerController::class, 'store']);
    Route::post('/follow-page', [UserFollowPageController::class, 'store']);
});

# laravel-jwt-auth

1.Clone The git repository<br>
2.Rename The .env file.<pre>cp .env.example .env</pre><br>
3.create a database and set the database details in the .env file.<br>
4.Run <pre>composer update</pre>
5.Run <pre>php artisan key:generate</pre>
6.Run <pre>php artisan jwt:secret</pre>
7.Run <pre>php artisan migrate</pre>
7.Run <pre>php artisan serve</pre>
8.Follow the api documentation<pre>https://documenter.getpostman.com/view/6464397/UVksMZqv




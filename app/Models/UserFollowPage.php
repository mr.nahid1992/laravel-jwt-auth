<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserFollowPage extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id', 'user_follow_page_id'
    ];
}
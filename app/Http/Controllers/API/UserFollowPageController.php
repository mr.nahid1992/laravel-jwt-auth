<?php

namespace App\Http\Controllers\API;



use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use  App\Models\UserFollowPage;
use App\Models\Page;
use Validator;

class UserFollowPageController extends BaseController
{
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'user_follow_page_id' => 'required|numeric',

        ]);
        if ($validator->fails()) {
            return $this->sendError($validator->errors());
        }

        $user_id = auth()->user()->getAuthIdentifier();
        $page_info = Page::find($request->user_follow_page_id);
        if (!$page_info) {
            return $this->sendError('The Follow Page Not exists', 'The Follow Page Not exists', 400);
        }

        $user_follower_page_info = UserFollowPage::where('user_id', $user_id)
            ->where('user_follow_page_id', $request->user_follow_page_id)
            ->get()->first();
        if ($user_follower_page_info) {
            $success['user_follow_page'] = $user_follower_page_info;
        } else {
            $input['user_id'] = $user_id;
            $user_follow_page = UserFollowPage::create($input);
            $success['user_follow_page'] = $user_follow_page;
        }

        return $this->sendResponse($success, 'Page Follow successfully .');
    }
}

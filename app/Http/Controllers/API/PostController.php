<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use  App\Models\Post;
use  App\Models\Page;
use Validator;

class PostController extends BaseController
{
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'post_content' => 'required',

        ]);
        if ($validator->fails()) {
            return $this->sendError($validator->errors());
        }
        $input['user_id'] = auth()->user()->getAuthIdentifier();
        $post = Post::create($input);

        $success['post'] = $post;
        return $this->sendResponse($success, 'Post successfully created.');
    }

    public function store_page_post(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'post_content' => 'required',
            'page_id' => 'required|numeric',

        ]);
        if ($validator->fails()) {
            return $this->sendError($validator->errors());
        }
        $user_id = auth()->user()->getAuthIdentifier();
        $page_details = Page::where('user_id', $user_id)
            ->where('id', $request->page_id)
            ->get()->first();

        if (!$page_details) {
            return $this->sendError('The User don"t have this page access ', 'The User don"t have this page access');
        }

        $input['user_id'] = $user_id;
        $post = Post::create($input);

        $success['post'] = $post;
        return $this->sendResponse($success, 'Post successfully created.');
    }
}

<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use  App\Models\UserFollower;
use Validator;

class UserFollowerController extends BaseController
{
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'user_follower_id' => 'required|numeric',

        ]);
        if ($validator->fails()) {
            return $this->sendError($validator->errors());
        }
        $user_id = auth()->user()->getAuthIdentifier();
        $user_info = User::find($request->user_follower_id);

        if(!$user_info){
            return $this->sendError('The Follow User Not exists', 'The Follow User Not exists', 400);
        }


        $user_follower_info = UserFollower::where('user_id', $user_id)
            ->where('user_follower_id', $request->user_follower_id)
            ->get()->first();

        if ($user_follower_info) {
            $success['user_follower'] = $user_follower_info;
        } else {
            $input['user_id'] = $user_id;
            $user_follower = UserFollower::create($input);
            $success['user_follower'] = $user_follower;


        }
        return $this->sendResponse($success, 'Follower successfully .');
    }
}

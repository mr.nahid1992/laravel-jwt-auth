<?php

namespace App\Http\Controllers\API;

use App\Models\Page;
use App\Models\Post;
use App\Models\UserFollower;
use App\Models\UserFollowPage;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Validator;


class AuthController extends BaseController
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('jwt.verify', ['except' => ['login', 'register']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|string|min:6',
        ]);

        if ($validator->fails()) {
            //return response()->json($validator->errors(), 422);
            return $this->sendError('Error validation', $validator->errors(), 422);
        }

        if (!$token = auth()->attempt($validator->validated())) {
            //return response()->json(['error' => 'Unauthorized'], 401);
            return $this->sendError('Invalid Credentials', ['error' => 'Unauthorised']);
        }
        $success['token'] = $token;
        $success['token_type'] = 'bearer';
        $success['user'] = auth()->user();

        return $this->sendResponse($success, 'User signed in');
        // return $this->createNewToken($token);
    }

    /**
     * Register a User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|string|between:2,100',
            'last_name' => 'required|string|between:2,100',
            'email' => 'required|string|email|max:100|unique:users',
            'password' => 'required|string|confirmed|min:6',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Error validation', $validator->errors(), 400);
            //return response()->json($validator->errors()->toJson(), 400);
        }

        /*$user = User::create(array_merge(
                    $validator->validated(),
                    ['password' => bcrypt($request->password)]
                ));

        return response()->json([
            'message' => 'User successfully registered',
            'user' => $user
        ], 201);*/

        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $success['user'] = $user;
        return $this->sendResponse($success, 'User successfully registered.');
    }


    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();
        $success = [];
        return $this->sendResponse($success, 'User successfully signed out.');
        //return response()->json(['message' => 'User successfully signed out']);
    }


    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function userProfile(Request $request)
    {
        $user_id = auth()->user()->getAuthIdentifier();
        $pageSize = $request->page_size;
        $page_id = $request->page_id;
        $success['my_pages'] = Page::where('user_id', $user_id)->where('id', $page_id)->paginate($pageSize);
        //$success['my_postes'] = Post::where('user_id', $user_id)->paginate(10);
        //$success['my_follow_user'] = UserFollower::where('user_id', $user_id)->paginate(10);
        //$success['my_follow_user'] = UserFollowPage::where('user_id', $user_id)->paginate(10);
        //$success['user'] = auth()->user();
        return $this->sendResponse($success, 'User Infromation Get Successfully.');
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    /* protected function createNewToken($token){
         return response()->json([
             'access_token' => $token,
             'token_type' => 'bearer',
             'expires_in' => auth()->factory()->getTTL() * 60,
             'user' => auth()->user()
         ]);
     }*/

}

<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use  App\Models\Page;
use Validator;
class PageController extends BaseController
{
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'page_name' => 'required',

        ]);
        if($validator->fails()){
            return $this->sendError($validator->errors());
        }
        $input['user_id']=auth()->user()->getAuthIdentifier();
        $page = Page::create($input);

        $success['page']=$page;
        return $this->sendResponse($success, 'Page successfully created.');
    }
}
